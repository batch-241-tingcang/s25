// JSON Objects
/*
	- It stands for JavaScript Object Notation
	- Also used in other programming languages
	- Core Javascript has a built in JSON object that contains method for parsing JSON objects and converting strings into Javascript objects
	- Javascript objects are not to be confused with JSON
	- JSON is used for serializing different data types inbto bytes
	- SerialiZation is the process of converting data into a series of bytes foreasier transmission/transfer of information
	- A byte is a unit of data that is eight binary digits (1 and 0) that is used to represent a character(letters, numbers, or typographic symbols)
	- Parse: convert JSON string to JS object (tinatawag nya)
	- Stringify: lumabas as a string
	- Di mag work ang dot notation kasi di siya object, rather string lang sya
	- Syntax: (mukhang object lang sya pero isang string lang to)
	 	{
			"propertyA": "propertyA",
			"propertyB": "propertyB"
		}
*/

// JSON
// {
// 	"city": "Quezon City",
// 	"province": "Metro Manila",
// 	"country": "Philippines"
// }

//  JSON, Array
// "cities": [
// 	{"city": "Quezon City", "province": "Metro Manila", "country": "Philippines"},
// 	{"city": "Manila City", "province": "Metro Manila", "country": "Philippines"},
// 	{"city": "Makati City", "province": "Metro Manila", "country": "Philippines"},
// ]

// console.log(cities[0].city)

// PARSE: Converting JSON string into objects
// JSON.parse()
let batchesJSON = '[{"batchname": "Batch X"}, {"batchName": "Batch Y"}]'
console.log('Result from parse method:')
console.log(JSON.parse(batchesJSON));
// console.log(batchesJSON[0].batchName);

let stringifiedObject = '{"name": "Eric", "age": "9", "address": { "city": "Bonifacio Global City", "country": "Philippines" }}'

console.log(JSON.parse(stringifiedObject));

// STRINGIFY: Convert Objects into String(JSON)
// JSON.stringify

let data = {
	name: 'Gabryl',
	age: 61,
	address: {
		city: "New York",
		country: "USA"
	}
}

console.log(data);
console.log(typeof data);


let stringData = JSON.stringify(data)
console.log(stringData);
console.log(typeof stringData);





































